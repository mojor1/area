﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Figure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Figure.Tests
{
    [TestClass()]
    public class PolygonTests
    {
        [TestMethod()]
        public void PolygonNegativeSidesTest()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>(){ -10, 3, 2 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, -10, 4 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 6, 8, -10 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { -10, -10, -10 }, new List<double>())); 
        }

        [TestMethod()]
        public void PolygonLessThen3SidesWithMediansTest1()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 3 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 3}, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>(), new List<double>()));
        }

        [TestMethod()]
        public void PolygonMediansMoreOrLessThanNeedTest()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 10, 3, 2 }, new List<double>() { 3}));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 10, 4, 5 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 10, 4, 5, 8 }, new List<double>() { 7}));
        }

        [TestMethod()]
        public void PolygonSidesEqual0Test()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 0, 3, 2 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 0, 4 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 6, 8, 0 }, new List<double>()));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 0, 0, 0 }, new List<double>()));
        }

        [TestMethod()]
        public void PolygonNegativeAndZeroRadiusTest1()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 3, 4}, -3));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Polygon(new List<double>() { 5, 3, 4}, 0));
        }

        [TestMethod()]
        public void PoligonAreTest()
        {
            Assert.AreEqual(6, new Polygon(new List<double>() { 3, 5, 4 }, new List<double>()).Are());
            Assert.AreEqual(78.53981633974483, new Polygon(new List<double>(), 5).Are());
            Assert.AreEqual(12, new Polygon(new List<double>() { 3, 4, 3, 4 }, new List<double>() { 5 }).Are());
            Assert.AreEqual(40, new Polygon(new List<double>() { 3, 8, 3, 2 },  5 ).Are());
        }
    }
}