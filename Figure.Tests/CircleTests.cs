using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Figure.Tests
{
    [TestClass]
    public class CircleTsts
    {
        [TestMethod]
        public void CircleNegativeRadiusTest()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Circle(-10)) ;
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Circle(-5));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Circle(0));
        }

        [TestMethod]

        public void CircleAre()
        {
            Assert.AreEqual(Circle.Are(10), new Circle(10).Are());
            Assert.AreEqual(314.1592653589793, new Circle(10).Are());
            Assert.AreEqual(Circle.Are(5), new Circle(5).Are());
            Assert.AreEqual(78.53981633974483, new Circle(5).Are());
            Assert.AreEqual(Circle.Are(21), new Circle(21).Are()); 
            Assert.AreEqual(1385.4423602330987, new Circle(21).Are());
        }

        [TestMethod]
        public void CircleCircumference()
        {
            Assert.AreEqual(Circle.Circumference(10), new Circle(10).Circumference());
            Assert.AreEqual(62.83185307179586, new Circle(10).Circumference());
            Assert.AreEqual(Circle.Circumference(5), new Circle(5).Circumference());
            Assert.AreEqual(31.41592653589793, new Circle(5).Circumference());
            Assert.AreEqual(Circle.Circumference(21), new Circle(21).Circumference());
            Assert.AreEqual(131.94689145077132, new Circle(21).Circumference());
        }
    }
}
