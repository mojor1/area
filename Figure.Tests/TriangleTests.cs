﻿using Figure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Figure.Tests
{
    [TestClass()]
    public class TriangleTests
    {
        [TestMethod()]
        public void TriangleNegativeSidesTest()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(-10, 3, 2));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(5, -10, 4));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(6, 8, -10));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(-10, -10, -10));
        }

        [TestMethod()]
        public void TriangleZeroSidesTest()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(0, 3, 2));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(5, 0, 4));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(6, 8, 0));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Triangle(0, 0, 0));
        }

        [TestMethod()]
        public void TriangleErrorSidesTest()
        {
            Assert.ThrowsException<ArgumentException>(() => new Triangle(10, 3, 2));
            Assert.ThrowsException<ArgumentException>(() => new Triangle(5, 9, 4));
            Assert.ThrowsException<ArgumentException>(() => new Triangle(6, 8, 15));
        }

        [TestMethod()]

        public void TriangleAreTest()
        {
            Assert.AreEqual(Triangle.Are(10, 5, 7.0), new Triangle(10, 5, 7).Are());
            Assert.AreEqual(16.248077, Math.Round(new Triangle(10, 5, 7).Are(), 6));
            Assert.AreEqual(Triangle.Are(3, 5, 4.0), new Triangle(3, 5, 4).Are());
            Assert.AreEqual(6, Math.Round(new Triangle(3, 5, 4).Are())); 
            Assert.AreEqual(Triangle.Are(21, 10, 13.0), new Triangle(21, 10, 13).Are());// 6, 8, 10
            Assert.AreEqual(48.74423, Math.Round(new Triangle(21, 10, 13).Are(), 5));
        }

        [TestMethod()]
        public void TriangleTestTest()
        {
            Assert.AreEqual(Triangle.Test(3, 5, 4), new Triangle(3, 5, 4).Test());
            Assert.AreEqual(true, new Triangle(3, 5, 4).Test());
            Assert.AreEqual(Triangle.Test(3, 6, 4), new Triangle(3, 6, 4).Test());
            Assert.AreEqual(false, new Triangle(3, 6, 4).Test());
            Assert.AreEqual(Triangle.Test(3, 5, 4), new Triangle(3, 5, 4).Test());
            Assert.AreEqual(true, new Triangle(3, 5, 4).Test());

        }       
    }
}
