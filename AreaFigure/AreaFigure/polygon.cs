﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figure
{
    public class Polygon: Figure
    {
        private readonly Lazy<List<double>> sides = new Lazy<List<double>>();
        private readonly Lazy<List<double>> medians = new Lazy<List<double>>(); // medians that divide a polygon into non-intersecting triangles
        private readonly Lazy<double> radius = new Lazy<double>(); // inscribed circle radius

        public Polygon(List<double> sides, List<double> medians)
        {
            if (sides.Count >= 3)
            {
                if (sides.Count(i => i <= 0) == 0)
                {
                    this.sides = new Lazy<List<double>>(sides);
                }
                else throw new ArgumentOutOfRangeException("sides.Value of polygon can not be less or equal 0!");
            }
            else throw new ArgumentOutOfRangeException("Very few sides.Value");
            if (medians.Count == sides.Count - 3 && medians.Count!=0)
            {
                if (medians.Count(i => i <= 0) == 0)
                {
                    this.medians = new Lazy<List<double>>(medians);
                }
                else throw new ArgumentOutOfRangeException("Medians of polygon can not be less or equal 0!");

            }
            else if(medians.Count != 0 || medians.Count != sides.Count - 3) throw new ArgumentOutOfRangeException("too many medians");
        }

        public Polygon(List<double> sides, double radius)
        {
            if (sides.Count > 3)
            {
                if (sides.Count(i => i <= 0) == 0)
                {
                    this.sides = new Lazy<List<double>>(sides);
                }
                else throw new ArgumentOutOfRangeException("sides.Value of polygon can not be less or equal 0!");
            }
            else if(sides.Count!=0) throw new ArgumentOutOfRangeException("Very few sides.Value");
            if (radius <= 0)
            {
                throw new ArgumentOutOfRangeException("Radius can't be less or equal zero");
            }
            else
            {
                this.radius = new Lazy<double>(radius);
            }
        }

        public override double Are()
        {
            if (radius.IsValueCreated && sides.IsValueCreated)
            {
                return Semiperimeter()*radius.Value;
            }
    else if (radius.IsValueCreated)
            {
                return Circle.Are(radius.Value);
            }
            else if(sides.IsValueCreated && medians.IsValueCreated)
            {
                return MedianAre();
            }
            else
            {
                return Triangle.Are(sides.Value[0], sides.Value[1], sides.Value[2]);
            }
        }

        private double MedianAre()
        {
            int j = 0;
            double are = 0;
            for (int i = 0; i < sides.Value.Count; i++)
            {
                if (i == 0 || i == sides.Value.Count - 2)
                {
                    are += Triangle.Are(sides.Value[i], sides.Value[++i], medians.Value[j]);
                }
                else
                {
                    are += Triangle.Are(sides.Value[i], medians.Value[j], medians.Value[++j]);
                    --j;
                }
                
            }
            return are;
        }

        private double Semiperimeter()
        {
            return sides.Value.Sum(i => i) / 2;
        }
    }
}
