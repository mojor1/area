﻿using System;

namespace Figure
{
    public class Circle:Figure
    {
        public Circle(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentOutOfRangeException("Radius can't be less or equal zero");
            }
            else
            {
                this.radius = radius;
            }
        }

        private double radius;
        
        public static double Are(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentOutOfRangeException("Radius can't be less or equal zero");
            }
            else
            {
                return Math.PI * (radius * radius);
            }
        }

        public override double Are()
        {           
            return Math.PI * (radius * radius);
        }

        public double Circumference()
        {
            return 2 * Math.PI * radius;
        }

        public static double Circumference(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentOutOfRangeException("Radius can't be less then zero");
            }
            else
            {
                return 2 * Math.PI * radius;
            }
        }
    }
}

