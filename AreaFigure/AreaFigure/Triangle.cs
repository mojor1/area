﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Figure
{
    public class Triangle:Figure
    {
        private double side1;
        private double side2;
        private double side3;

        public Triangle(double side1, double side2, double side3)
        {
            if (side1 <= 0 || side2 <= 0 || side3 <= 0)
            {
                throw new ArgumentOutOfRangeException("Sides of triangle can not be less or equal 0!");
            }
            else
            {
                double[] sides = new[] { side1, side2, side3 };
                if (sides.First(i => i != sides.Max() && i != sides.Min()) + sides.Min() <= sides.Max())
                {
                    throw new ArgumentException("No such triangle exists");
                }
                else
                {
                    this.side1 = side1;
                    this.side2 = side2;
                    this.side3 = side3;
                }
            }
        }

        public static double Are(double side1, double side2, double side3)
        {
            if (side1 <= 0 || side2 <= 0 || side3 <= 0)
            {
                throw new ArgumentOutOfRangeException("Sides of triangle can not be less or equal 0!");
            }
            else
            {
                double p = (side1 + side2 + side3) / 2;
                double are = Math.Sqrt(p * (p - side1) * (p - side2) * (p - side3));
                if (are == double.NaN)
                {
                    throw new ArgumentException("No such triangle exists");
                }
                else return are;
            }
        }
        public static double Are(double side1, double side2, int corner)
        {
            if (side1 <= 0 || side2 <= 0)
            {
                throw new ArgumentOutOfRangeException("Sides of triangle can not be less or equal 0!");
            }
            else if (corner <= 0)
            {
                throw new ArgumentOutOfRangeException("Coner of triangle can not be less or equal 0!");
            }
            else
            {

                return 0.5 * side1 * side2 * Math.Sin(corner * Math.PI / 180);
            }
        }

        public override double Are()
        {            
                double p = (side1 + side2 + side3) / 2;
             return Math.Sqrt(p * (p - side1) * (p - side2) * (p - side3)); 
        }
        public static bool Test(double side1, double side2, double side3)
        {
            if (side1 <= 0 || side2 <= 0 || side3 <= 0)
            {
                throw new ArgumentOutOfRangeException("Sides of triangle can not be less or equal 0!");
            }
            else
            {
                if ((side1 * side1 + side2 * side2 == side3 * side3) || (side1 * side1 + side3 * side3 == side2 * side2) || (side3 * side3 + side2 * side2 == side1 * side1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool Test()
        {
                if ((side1 * side1 + side2 * side2 == side3 * side3) || (side1 * side1 + side3 * side3 == side2 * side2) || (side3 * side3 + side2 * side2 == side1 * side1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }
    }
}
