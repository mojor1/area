﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Figure
{
    public abstract class Figure
    {
        public abstract double Are(); 
    }
}
